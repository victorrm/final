import transforms
import sys
import images
def main():
    
    name = sys.argv[1]
    split = name.split(".jpg")
    funcion = sys.argv[2]
    image = images.read_img(name)

    if funcion == "change_colors":
        colores_viejos = (int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]))
        colores_nuevos = (int(sys.argv[6]), int(sys.argv[7]), int(sys.argv[8]))
        imagen = transforms.change_colors(image, colores_viejos, colores_nuevos)

    elif funcion == "rotate_right":
        imagen = transforms.rotate_right(image)

    elif funcion == "rotate_colors":
        nrotac = int(sys.argv[3])
        imagen = transforms.rotate_colors(image, nrotac)

    elif funcion == "mirror":
        imagen = transforms.mirror(image)

    elif funcion == "blur":
        imagen = transforms.blur(image)

    elif funcion == "crop":
         x = int(sys.argv[3])
         y = int(sys.argv[4])
         ancho = int(sys.argv[5])
         alto = int(sys.argv[6])
         imagen = transforms.crop(image, x, y, ancho, alto)

    elif funcion == "filter":
        r = float(sys.argv[3])
        g = float(sys.argv[4])
        b = float(sys.argv[5])
        imagen = transforms.filter(image, r, g, b)

    elif funcion == "grayscale":
        imagen = transforms.grayscale(image)

    elif funcion == "shift":
        horizontal, vertical = int(sys.argv[3]), int(sys.argv[4])
        imagen = transforms.shift(image, horizontal, vertical)

    else:
        print("funcion o enumeracion erronea")
        sys.exit(1)

    images.write_img(imagen, f"{split[0]}_trans.jpg")


if __name__ == '__main__':
    main()








