import transforms
import sys
import images

def main():
    count = 0
    name = sys.argv[1]
    count += 1
    split = name.split(".jpg")
    numero = split.count("")

    if numero < 1:
        print("revisa el texto")
        sys.exit(1)

    image = images.read_img(name)
    argcontar = len(sys.argv) - 1

    while count < argcontar:
        funcion = sys.argv.pop(2)
        count += 1

        if funcion == "change_colors":
            color_viejo = (int(sys.argv.pop(2)), int(sys.argv.pop(2)), int(sys.argv.pop(2)))
            color_nuevo = (int(sys.argv.pop(2)), int(sys.argv.pop(2)), int(sys.argv.pop(2)))
            count += 6
            image = transforms.change_colors(image, color_viejo, color_nuevo)

        elif funcion == "rotate_right":
            image = transforms.rotate_right(image)

        elif funcion == "rotate_colors":
            n_rotations = int(sys.argv.pop(2))
            count += 1
            image = transforms.rotate_colors(image, n_rotations)

        elif funcion == "mirror":
            image = transforms.mirror(image)

        elif funcion == "blur":
            image = transforms.blur(image)

        elif funcion == "crop":
            x = int(sys.argv.pop(2))
            y = int(sys.argv.pop(2))
            width = int(sys.argv.pop(2))
            height = int(sys.argv.pop(2))
            count += 4
            image = transforms.crop(image, x, y, width, height)

        elif funcion == "filter":
            r = float(sys.argv.pop(2))
            g = float(sys.argv.pop(2))
            b = float(sys.argv.pop(2))
            count += 3
            image = transforms.filter(image, r, g, b)

        elif funcion == "grayscale":
            image = transforms.grayscale(image)

        elif funcion == "shift":
            horizontal, vertical = int(sys.argv.pop(2)), int(sys.argv.pop(2))
            count += 2
            image = transforms.shift(image, horizontal, vertical)

        else:
            print("Operación no válida.")
            sys.exit(1)

        images.write_img(image, f"{split[0]}_trans.jpg")


if __name__ == '__main__':
    main()




