import images
def change_colors(image_colors: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    for column in image_colors:
        for imagen in range(len(column)):
            if column[imagen] == to_change:
                column[imagen] = to_change_to

    return image_colors


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    image_rotated = []
    for column in image:
        new_column = []
        for pixel in column:
            r = (pixel[0] + increment) % 256
            g = (pixel[1] + increment) % 256
            b = (pixel[2] + increment) % 256
            new_column.append((r, g, b))
        image_rotated.append(new_column)

    return image_rotated


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rotated_image = []
    for row in zip(*image):
        rotated_image.append(list(reversed(row)))

    return rotated_image


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    image_mirror = []
    for pixel in image[::-1]:
        image_mirror.append(pixel)

    return image_mirror


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    image_filter = []
    for column in image:
        new_column = []
        for pixel in column:
            r1 = min(int(pixel[0] * r), 255)
            g1 = min(int(pixel[1] * g), 255)
            b1 = min(int(pixel[2] * b), 255)
            new_column.append((r1, g1, b1))
        image_filter.append(new_column)

    return image_filter

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    image_blur = [[(0, 0, 0) for _ in range(len(image[0]))] for _ in range(len(image))]

    for x in range(len(image)):
        for y in range(len(image[0])):
            pixel = []
            for dx in [-1, 0, 1]:
                for dy in [-1, 0, 1]:
                    nx, ny = x + dx, y + dy
                    if 0 <= nx < len(image) and 0 <= ny < len(image[0]):
                        pixel.append(image[nx][ny])
            r = sum(p[0] for p in pixel) // len(pixel)
            g = sum(p[1] for p in pixel) // len(pixel)
            b = sum(p[2] for p in pixel) // len(pixel)
            image_blur[x][y] = (r, g, b)

    return image_blur


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:

    rows, cols = len(image), len(image[0])

    image_shift = [
        [image[(x - horizontal) % rows][(y - vertical) % cols] for y in range(cols)]
        for x in range(rows)
    ]
    return image_shift





def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    crop_x = x + width
    crop_y = y + height
    image_crop = []
    for pixel in range(x, crop_x):
        lista = []
        for todo in range(y, crop_y):
            valor = image[pixel][todo]
            lista.append(valor)
        image_crop.append(lista)

    return image_crop

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    image_grayscale = []
    for column in image:
        new_column = []
        for pixel in column:
            media = int(sum(pixel) / 3)
            new_column.append((media, media, media))
        image_grayscale.append(new_column)

    return image_grayscale








