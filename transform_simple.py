import transforms
import sys
import images
def main():
    name = sys.argv[1]
    split = name.split(".jpg")
    funcion = sys.argv[2]
    image = images.read_img(name)

    if funcion == "mirror":
        imagen = transforms.mirror(image)

    elif funcion == "rotate_right":
        imagen = transforms.rotate_right(image)

    elif funcion == "grayscale":
        imagen = transforms.grayscale(image)

    elif funcion == "blur":
        imagen = transforms.blur(image)

    else:
        print("funcion erronea")
        sys.exit(1)

    images.write_img(imagen, f"{split[0]}_trans.jpg")

if __name__ == '__main__':
    main()






